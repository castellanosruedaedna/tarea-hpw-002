function ejercicio10(numerosArray) {

	function mayor(array){	
	    return (Math.max.apply(Math,array));
	}
	function menor(array){
		 return (Math.min.apply(Math,array));
	}
	function promedio(array){
		var promedio=0;
		for (var i = 0; i < array.length; i++) {
			promedio+=array[i];

		}
		return (promedio/array.length);
	}

	return{
		mayor:mayor(numerosArray),
		menor:menor(numerosArray),
		promedio:promedio(numerosArray)
	};


}

